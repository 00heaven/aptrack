import { DataService } from './services/data.service';
import { Component } from '@angular/core';
// import { DataService}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = true;
  constructor(private dataServ : DataService){
    this.dataServ.title.subscribe((value) => {
      this.title = value;
    })
  }
}
