import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userdata: any = {};
  users: any[];
  dataLoaded: boolean = false;

  localData = [];
  globalData = [];
  constructor(public dataServ : DataService) { 
    this.dataServ.userData.subscribe((data) => {
      this.userdata = data;
      console.log(data);
    })
  }

  ngOnInit() {
    this.dataServ.userDataFetch().subscribe((users) => {
      console.log(users);
      this.users = users;
      this.dataLoaded = true;
    

    this.dataServ.fetchDateWise().subscribe((data) => {
      console.log(data[this.users["pincode"]]);
      for(let i in data[this.users["pincode"]]){
        console.log(i)
        this.localData.push(data[this.users["pincode"]][i]);
      }

      for(let i in data){
        for(let j in data[i]){
          this.globalData.push(data[i][j]);
        }
      }
      console.log(this.globalData.sort(function(a,b){return a["amountCO"]/a["duration"] - b["amountCO"]/b["duration"]}));
      console.log(this.localData)
    })
  })
  }

}
