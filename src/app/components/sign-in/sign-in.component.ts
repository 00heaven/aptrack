import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  username: string;
  data: {};
  constructor(public dataServ: DataService) { }

  ngOnInit() {
  }

  login(){
    this.dataServ.userDataFetch().subscribe((users) => {
      // console.log(users)
      users.forEach((user) => {
        if(user["userName"] == this.username){
          this.data = user;
          console.log("going inside" , user);
          this.dataServ.userData.next(user);
          this.dataServ.title.next(false);
        }
      })
    })
  }

}
