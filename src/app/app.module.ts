import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { HomeComponent } from './components/home/home.component';

import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { RouterModule, Router } from '@angular/router';
import { environment } from '../environments/environment';





@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomeComponent,
    // 
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    CardModule, TableModule, ButtonModule,
    RouterModule.forRoot([
      {path: 'home', component: HomeComponent },
      {path: 'signIn', component: SignInComponent} //, canActivate: [AuthGuard]
    ]),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
