import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  title = new Subject<boolean>();
  userData = new Subject<any>();

  constructor(public angularDb : AngularFirestore) {
    
   }

  userDataFetch() {
    console.log("Came Here")
    return this.angularDb.collection('users').valueChanges();
  }


  localUserDataFetch(pincode){
      return this.angularDb.collection('drives').doc(pincode).valueChanges();
  }

  fetchDateWise(){
    let date = new Date();
    let time = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
    return this.angularDb.collection('datewise').doc(time).valueChanges();
  }
}
