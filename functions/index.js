const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

var db = admin.firestore();


exports.newUser = functions.https.onRequest((request, response) => {
    let username = request.body.username;
    let vehicle = request.body.vehicleNo;
    let address = request.body.address;
    let mobileNo = request.body.mobileNo;
    let userId = username + ":" + vehicle;
    let pincode = request.body.pincode;
    let date = new Date();
    let time = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
    
    db.collection('users').doc(userId).set({
        'vehicleNo' : vehicle,
        'userName' : username,
        'address' : address,
        'mobileNo' : mobileNo,
        'userId' : userId,
        'pincode' : pincode
    }).then((value) => {
        response.status(200).send("The user is created Successfully");
        db.collection('drives').doc(pincode).set({
            [userId] : {
                [time] : {
                    'duration' : 0,
                    'amountCO' : 0
                }
            }
        },{merge: true});
        db.collection('datewise').doc(time).set({
            [pincode] : {
                [userId] : {
                    'duration' : 0,
                    'amountCO' : 0
                }
            }
        },{merge: true});
    })
})


exports.pollutionData = functions.https.onRequest((request, response) => {
    let username = request.body.username;
    let vehicle = request.body.vehicleNo;
    let duration = request.body.time;
    let amountCO = request.body.amount;
    let userId = username + ":" + vehicle;
    let date = new Date();
    let time = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
    
    db.collection('users').doc(userId).get().then((doc) => {
        let user = doc.data();
        let data = {};
        data[vehicle] = {};
        data[vehicle][time] = {
            "duration" : duration,
            "amountCO" : amountCO
        };
        db.collection('drives').doc(user["pincode"]).get().then((value) => {
            let data = value.data();
            if(data[userId][time] != undefined){
                data[userId][time]["duration"] += duration;
                data[userId][time]["amountCO"] += amountCO;
            }
            db.collection('drives').doc(user["pincode"]).set(data,{merge: true}).then((value) => {
                response.status(200).send("The local leaderboard data updated");
            })
        })

        db.collection('datewise').doc(time).get().then((value) => {
            let data = value.data();
            if(data[user["pincode"]][userId] != undefined){
                data[user["pincode"]][userId]["duration"] += duration;
                data[user["pincode"]][userId]["amountCO"] += amountCO;
            }
            db.collection('datewise').doc(time).set(data,{merge: true}).then((value) => {
                response.status(200).send("The local leaderboard data updated");
            })
        })
    })
})

exports.fetchUser = functions.https.onRequest((request,response) => {
    let username = request.body.username;
        db.collection('users').get().then((data) => {
            data.forEach((user) => {
                user = user.data();
                if(user["userName"] == username){
                    response.send(user);
                }
            })
            response.send(null);
        })
})